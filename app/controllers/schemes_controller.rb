class SchemesController < ApplicationController
  before_action :set_scheme, only: %i[absolute_returns cagr_returns]

  def index
    # handle index view
  end

  def absolute_returns
    profit_amount = AbsoluteReturnsService.new(returns_params).call
    render json: { profit: profit_amount }
  rescue StandardError => e
    render json: { error:  e }, status: :internal_server_error
  end

  def cagr_returns
    profit_amount = CagrService.new(returns_params).call
    render json: { profit: profit_amount }
  rescue StandardError => e
    render json: { error:  e.message }, status: :internal_server_error
  end

  def xirr_returns
    profit_amount = XirrService.new(xirr_params).call
    render json: { data: profit_amount }
  rescue StandardError => e
    render json: { error: e }, status: :internal_server_error
  end

  private

  def returns_params
    permitted_params = params.permit(:scheme_id, :invested_date, :invested_amount)
    permitted_params[:scheme] = @scheme
    permitted_params
  end

  def xirr_params
    params.require(:scheme).permit(xirr: %i[transaction_date amount])
  end

  def set_scheme
    @scheme = Scheme.find_by(id: params[:scheme_id])
    unless @scheme.present?
      render json: {
        error: 'No scheme found for give ID'
      }, status: :bad_request and return
    end
    @scheme
  end
end

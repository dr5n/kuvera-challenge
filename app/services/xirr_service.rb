class XirrService
  include Xirr

  def initialize(params)
    @periodic_investments = params[:xirr]
    @total_investement = total_investment(params[:xirr])
    @response = OpenStruct.new
  end

  def call
    @response.total_investement = @total_investement
    @response.return = total_profit_amount.to_i
    @response.xirr_percent = xirr_percent
    @response.earnings = profit_amount
    @response.to_h
  end

  private

  def xirr_percent
    cash_flow = Cashflow.new
    @periodic_investments.each do |investment|
      cash_flow << Transaction.new(investment['amount'].to_i, date: investment[:transaction_date].to_date)
    end
    cash_flow.xirr
  end

  def total_investment(investments)
    total_investments = 0
    investments.each do |investment|
      total_investments += investment['amount'].to_i
    end
    total_investments
  end

  def profit_amount
    (xirr_percent / 100) * @total_investement
  end

  def total_profit_amount
    @total_investement + profit_amount
  end
end

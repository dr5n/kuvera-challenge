module SchemesHelper
  def self.nav_of_the_day(scheme, nav_date, search_type = 'past')
    statistic = fetch_statistic(scheme, nav_date)
    return statistic.first.nav if statistic.present?
    until statistic.present?
      if search_type == 'past'
        nav_date += 1
      else
        nav_date -= 1
      end
      statistic = fetch_statistic(scheme, nav_date)
    end
    statistic.first.nav
  end

  def self.fetch_statistic(scheme, invested_date)
    scheme.statistics.where(nav_date: invested_date)
  end
end

class CreateSchemes < ActiveRecord::Migration[5.1]
  def change
    create_table :schemes do |t|
      t.integer :scheme_code
      t.string :scheme_name

      t.timestamps
    end
  end
end

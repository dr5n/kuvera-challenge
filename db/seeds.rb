# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'csv'
scheme_count = 0
CSV.read(Rails.root.join('nav_data.csv')).each do |row|
  next if row.count < 2
  p row
  break if scheme_count == 20 # take some first 20 fund's statistics
  scheme = Scheme.find_by_name(row[1])
  if scheme.present?
    scheme.statistics.create(nav: row[2], repurchase_price: row[3], sale_price: row[4], nav_date: Date.parse(row[5]))
  else
    new_scheme = Scheme.create(code: row[0], name: row[1])
    new_scheme.statistics.create(nav: row[2], repurchase_price: row[3], sale_price: row[4], nav_date: Date.parse(row[5]))
    scheme_count += 1
  end
end
